package sample;

/**
 * Created by ishraf222 on 23/02/16.
 */
public class StudentRecord {



    private String id;

    public float getFinalGrade() {
        return finalGrade;
    }

    public void setFinalGrade(float finalGrade) {
        this.finalGrade = finalGrade;
    }

    public char getLetterGrade() {
        return letterGrade;
    }

    public void setLetterGrade(char letterGrade) {
        this.letterGrade = letterGrade;
    }

    private float finalGrade;
    private char letterGrade;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getMidterm() {
        return midterm;
    }

    public void setMidterm(float midterm) {
        this.midterm = midterm;
    }

    public float getAssignment() {
        return assignment;
    }

    public void setAssignment(float assignment) {
        this.assignment = assignment;
    }

    public float getFinalExam() {
        return finalExam;
    }

    public void setFinalExam(float finalExam) {
        this.finalExam = finalExam;
    }

    private float midterm;
    private float assignment;
    private float finalExam;


    public StudentRecord(String sid, float ass, float mid, float finalE)
    {
        this.id = sid;
        this.assignment = ass;
        this.midterm = mid;
        this.finalExam = finalE;

        this.finalGrade = this.assignment * 0.2f + this.midterm * 0.3f + this.finalExam * 0.5f;
        this.letterGrade = determineGrade(this.finalGrade);

    }

    public char determineGrade(float finale)
    {
        if(finale < 50.f)
        {
            return 'F';
        }
        if(finale >49  && finale <60 )
        {
            return 'D';
        }

        if(finale >59  && finale <70 )
        {
            return 'C';
        }
        if(finale >69  && finale <80 )
        {
            return 'B';
        }
        if(finale >79)
        {
            return 'A';
        }
        else
            return '*';


    }
}
