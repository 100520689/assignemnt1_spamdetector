package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.*;

public class WordCounter {
    //_____Variable setup_____//
    public Map<String, Integer> getWordCounts() {
        return wordCounts;
    }

    private Map<String,Integer> wordCounts;
    private Map<String, Integer> mailCount;


    private Map<String , Double> hamFreq; //Pr(Wi|H)
    private Map<String, Double> spamFreq;// Pr(Wi|S)
    private Map<String, Double> probabiltyMap; /// Pr(S|Wi)


    public ArrayList<TestFile> testProbSpam;

    private double accuracy = 0;
    private double precision = 0;
    private double fullcount = 0;
    private int totalCount = 0;
    private int totalCount2= 0;


    public WordCounter() {
        wordCounts = new TreeMap<>();
        mailCount = new TreeMap<>();
        hamFreq = new TreeMap<>();
        spamFreq = new TreeMap<>();
        probabiltyMap = new TreeMap<>();
        testProbSpam = new ArrayList<TestFile>();

    }

    public double getAccuracy() {
        return accuracy;
    }

    public double getPrecision() {
        return precision;
    }

    //_____Reads in the training files and scans in the words_____//
    public void processFileMailCount(File file)throws IOException {
        if (file.isDirectory()) {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();
            for (int i = 0; i < filesInDir.length; i++) {
                processFileMailCount(filesInDir[i]);
            }
        } else if (file.exists()) {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);

            mailCount.clear();
            totalCount ++;

            while (scanner.hasNext()) {
                String word = scanner.next();

                if (isWord(word)) {
                    countmail(word);
                }
            }
        }
    }

    //_____Reads in the testing ham files and scans in the words_____//
    public void processTestFileHam(File file)throws IOException
    {
        if (file.isDirectory())
        {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();
            for (int i = 0; i < filesInDir.length; i++) {
                 processTestFileHam(filesInDir[i]);
            }
        } else if (file.exists())
        {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);

            //create testfile object
            TestFile temp = new TestFile(file.getName(),0.0, "ham");

            //mailCount.clear();
            totalCount ++;
            calcTestProbHam(scanner, temp);

        }
    }

    //_____Reads in the training spam files and scans in the words_____//
    public void processTestFileSpam(File file)throws IOException
    {
        if (file.isDirectory())
        {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();
            for (int i = 0; i < filesInDir.length; i++) {
                processTestFileHam(filesInDir[i]);
            }
        } else if (file.exists())
        {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);

            TestFile temp = new TestFile(file.getName(),0.0, "spam");

            //mailCount.clear();

            totalCount2++;
            calcTestProbSpam(scanner, temp);

        }
    }

    //_____Calculate Pr(Wi | S)_____//
    public void calcSpamFreq()
    {
        //retrieve key set to setup iterator
        Set<String> words = wordCounts.keySet();
        Iterator<String> wordIterator = words.iterator();

        //use worditerator to retrieve the words.
        while (wordIterator.hasNext())
        {
            String word = wordIterator.next();

            double num = wordCounts.get(word);

            //System.out.println("spam word: " + word + " " + num/totalCount);

            spamFreq.put(word,num/501);
        }

    }

    //_____Calculate Pr(Wi | H)_____//
    public void calcHamFreq()
    {
        //retrieve key set to setup iterator
        Set<String> words = wordCounts.keySet();
        Iterator<String> wordIterator = words.iterator();

        //use worditerator to retrieve the words.
        while (wordIterator.hasNext())
        {
            String word = wordIterator.next();

            double num = wordCounts.get(word);

            //System.out.println("ham word: " + word + " " + num/totalCount);

            hamFreq.put(word,num/2501);
        }

    }

    //_____Calculate Pr(S | Wi)_____//
    public void calcProbFreq()
    {
        //retrieve key set to setup iterator
        Set<String> word = hamFreq.keySet();
        Iterator<String> wordIter = word.iterator();

        double num;
        //use worditerator to retrieve the words.
        while (wordIter.hasNext())
        {
            String test = wordIter.next();

            //check if spamfrequency contains the word test
            if(spamFreq.containsKey(test))
            {
                //calculate and store in Probabilitymap
                num = spamFreq.get(test) / (spamFreq.get(test) + hamFreq.get(test));
                probabiltyMap.put(test,num);
                //System.out.println("probility of word: " + test + " " + num);
            }
            else
            {
                num = 0.0;
                probabiltyMap.put(test,num);
                //System.out.println("probility of word: " + test + " " + 0.0);

            }

        }

    }

    //_____Calculate Pr(S | F) for ham_____//
    public void calcTestProbHam(Scanner scan, TestFile SF)
    {
        double n = 0 ;

        //use the scanner to retrieve the words of each files
        while (scan.hasNext())
        {
            String testWord = scan.next();

            if(isWord(testWord))
            if(probabiltyMap.containsKey(testWord))
            {
                //ln(x) cant have 0 or 1 values so we ignore them.
                if(probabiltyMap.get(testWord) == 1 || probabiltyMap.get(testWord) == 0){}
                else
                n += Math.log((1 - probabiltyMap.get(testWord)) -
                    Math.log(probabiltyMap.get(testWord))); //calculate n

            }

        }

        //System.out.println("n : " + n);

        //_____calculate 1/1 + e^n_____//
         double prob = 1 /(1 + (Math.pow(Math.E,n)));

        //System.out.println("prob :"  + prob);

        SF.setSpamProbability(prob);
        SF.setActualClass("Ham");

        //System.out.println("Filename : " + SF.getFilename());

       // System.out.println(SF.getSpamProbRounded());

        testProbSpam.add(SF);


    }

    //_____Calculate Pr(S | F) for spam_____//
    public void calcTestProbSpam(Scanner scan, TestFile SF)
    {
        double n = 0 ;

        //use the scanner to retrieve the words of each files
        while (scan.hasNext())
        {
            String testWord = scan.next();

            if(isWord(testWord))
                if(probabiltyMap.containsKey(testWord))
                {
                    //ln(x) cant have 0 or 1 values so we ignore them.
                    if(probabiltyMap.get(testWord) == 1 || probabiltyMap.get(testWord) == 0){}
                    else
                        n += Math.log((1 - probabiltyMap.get(testWord)) -
                                Math.log(probabiltyMap.get(testWord))); //calculate n


                }

        }

        //System.out.println("n : " + n);

        //_____calculate 1/1 + e^n_____//
        double prob = 1 /(1 + (Math.pow(Math.E,n)));

        //System.out.println("prob :"  + prob);

        SF.setSpamProbability(prob);
        SF.setActualClass("Spam");

        //System.out.println("Filename : " + SF.getFilename());

        // System.out.println(SF.getSpamProbRounded());

        testProbSpam.add(SF);


    }

    public ObservableList<TestFile> getAllTestFiles()
    {
        ObservableList<TestFile> files =
                FXCollections.observableArrayList();

        double accuracy_value = 0;
        double precision_value = 0;
       // System.out.println(totalCount + " " + totalCount2);
        //loop through the arraylist of testfiles
        for (int i = 1 ; i < testProbSpam.size(); i ++)
        {
            //parse from string to double
            double testprob = Double.parseDouble(testProbSpam.get(i).getSpamProbRounded());

            fullcount++;

            //if(totalCount<){class = "Ham";}
           // else{class = "Spam";}
            files.add(new TestFile(testProbSpam.get(i).getFilename(), testprob, testProbSpam.get(i).getActualClass()));

           // System.out.println(testProbSpam.get(i).getActualClass());

            //check file type and probability to update accuracy & precision
            if(testProbSpam.get(i).getActualClass() == "Ham" && testProbSpam.get(i).getSpamProbability() < 0.5)
            {
                accuracy_value++;
            }
            if(testProbSpam.get(i).getActualClass() == "Spam" && testProbSpam.get(i).getSpamProbability() > 0.5)
            {
                accuracy_value++;
                precision_value++;
            }

        }

        //calculate accuracy and precision
        accuracy = accuracy_value/fullcount;
        precision = precision_value/fullcount;
        //System.out.println("Acc: " + accuracy + "Pre: " + precision);

        return files;
    }

    //_____calculate word count of each file_____//
    private void countmail(String word)
    {
        if(!(mailCount.containsKey(word)))
        {
            if (wordCounts.containsKey(word))
            {
                int oldCount = wordCounts.get(word);
                wordCounts.put(word, oldCount + 1);
            }
            else
            {
                wordCounts.put(word, 1);
            }
            mailCount.put(word,1);
        }

    }

    //_____check if the word is made up of a->z characters in alphab. too_____//
    private boolean isWord(String str){
        String pattern = "^[a-zA-Z]*$";
        if (str.matches(pattern)){
            return true;
        }
        return false;
    }

    //_____print the words out to file specified_____//
    public void printWordCounts(int minCount, File outputFile) throws FileNotFoundException {
        System.out.println("Saving word counts to " + outputFile.getAbsolutePath());
        if (!outputFile.exists() || outputFile.canWrite()) {
            PrintWriter fout = new PrintWriter(outputFile);

            Set<String> keys = wordCounts.keySet();
            Iterator<String> keyIterator = keys.iterator();

            while(keyIterator.hasNext()) {
                String key = keyIterator.next();
                int count = wordCounts.get(key);

                if (count >= minCount) {
                    //System.out.println(key + ": " + count);
                    fout.println(key + ": " + count);
                }
            }
            fout.close();
        } else {
            System.err.println("Cannot write to output file");
        }
    }


}