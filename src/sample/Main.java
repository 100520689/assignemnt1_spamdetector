/*

BY ISHRAF UDDIN 100520689
BY NOUR HALABI 100525020

 */
package sample;

import javafx.application.Application;
import javafx.beans.binding.StringBinding;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


public class Main extends Application {
    private Stage window;
    private BorderPane layout;
    private TableView<TestFile> table;
    private TextField sidField, assField, midField,finalExField,finalMarkFieldm, gradeField;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Assignment1_Ishraf_&_Nour");
        primaryStage.setScene(new Scene(root, 600, 500));
        primaryStage.show();

        //_____Setup directory choose in order to select the data folder_____//
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File("."));
        File mainDirectory = directoryChooser.showDialog(primaryStage);


        //_____Create an object of class wordCounted in order to access its functions_____//
        WordCounter wc = new WordCounter();
        //WordCounter wc2 = new WordCounter();


        //_____Get the file path from mainDirectory_____//
        File trainSpamDir = new File(mainDirectory.getPath() + "/train/spam");
        File trainHamDir = new File(mainDirectory.getPath() + "/train/ham");

        File testSpamDir = new File(mainDirectory.getPath() + "/test/spam");
        File testHamDir = new File (mainDirectory.getPath()+ "/test/ham");

        //System.out.println(mainDirectory.getPath() + "File: " + dataDir);


        try {
            //_____Load in training spam emails_____//
            wc.processFileMailCount(trainSpamDir);
            //  wc.printWordCounts(2,new File(mainDirectory.getPath() + "/right_outs/mail_results_Spam2"));

            //_____Calculate Pr(Wi | S)_____//
            wc.calcSpamFreq();

            wc.processFileMailCount(trainHamDir);
            // wc.printWordCounts(2, new File(mainDirectory.getPath() + "/right_outs/mail_results_Ham"));

            //_____Calculate Pr(Wi | H)_____//
            wc.calcHamFreq();

            //_____Calculate Pr(S | Wi)_____//
            wc.calcProbFreq();

            //_____Calculate Pr(S | F) for ham and spam test files_____//
            wc.processTestFileHam(testHamDir);
            wc.processTestFileSpam(testSpamDir);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //_____Table Initialization_____//
        table = new TableView<>();
        table.setItems(wc.getAllTestFiles());
        table.setEditable(true);


        //_____Create table columns_____//
        TableColumn<TestFile, String> sidColumn = null;
        sidColumn = new TableColumn<>("Filename");
        sidColumn.setMinWidth(200);
        sidColumn.setCellValueFactory((new PropertyValueFactory<>("filename")));


        TableColumn<TestFile, String> assColumn = null;
        assColumn = new TableColumn<>("Class");
        assColumn.setMinWidth(100);
        assColumn.setCellValueFactory((new PropertyValueFactory<>("actualClass")));

        TableColumn<TestFile, Float> midColumn = null;
        midColumn = new TableColumn<>("Probability");
        midColumn.setMinWidth(200);
        midColumn.setCellValueFactory((new PropertyValueFactory<>("spamProbability")));


        //_____Create labels for accuracy and precision_____//
        Label acc = new Label("Accuracy: ");
        Label pre = new Label("Precision: ");
        TextField acc_text = new TextField();
        TextField prec_text = new TextField();

        acc_text.setText(wc.getAccuracy()+"");
        prec_text.setText(wc.getPrecision()+"");
       // System.out.println(wc.getAccuracy() + " " +  wc.getPrecision());

        //_____create gridpane in order to customize placement of UI_____//
        GridPane editArea = new GridPane();
        editArea.setPadding(new Insets(10, 10, 10, 10));
        editArea.setVgap(10);
        editArea.setHgap(10);
        editArea.add(acc, 0, 0);
        editArea.add(pre, 0, 1);
        editArea.add(acc_text, 1, 0);
        editArea.add(prec_text, 1, 1);

        table.setItems(wc.getAllTestFiles());
        table.getColumns().addAll(sidColumn,assColumn,midColumn);//,FinalM,LetterG);

        layout = new BorderPane();
        layout.setCenter(table);
        layout.setBottom(editArea);

        Scene scene = new Scene(layout, 600, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    };


    public static void main(String[] args) {
        launch(args);
    }
}
